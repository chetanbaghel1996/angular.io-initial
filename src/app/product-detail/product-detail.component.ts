import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { products } from '../products';
import { CartService } from './../cart/cart.service'

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  constructor(
    public route : ActivatedRoute,
    public cartService : CartService,
  ) { }

  product : any;

  ngOnInit(): void {
    let routeParams  = this.route.snapshot.paramMap;
    // let productId = +routeParams.get("productId");
    let productId = Number(routeParams.get("productId"));
    this.product = products.find(product=> product.id === productId)
  }

  addToCart(product) {
    this.cartService.addToCart(product);
    window.alert('Your product has been added to the cart!');
  }

}
