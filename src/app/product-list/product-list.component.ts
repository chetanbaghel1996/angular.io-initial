import { Component, OnInit } from '@angular/core';
import { products } from '../products';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  constructor() { }
  Products = products;
  ngOnInit(): void {
  }

  share(product){
    window.alert(product.name+" has been shared")
  }

  onNotify(){
    window.alert("You will be notified");
  }

}
